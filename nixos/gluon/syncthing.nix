{ config, pkgs, lib, ... }:

{
  services.syncthing = {
    enable = true;
    overrideDevices = true;
    overrideFolders = true;
    devices = {
      calyx-phone = {
        id = "NQUHJUO-TT52SZD-RA66XJU-V3SORVG-W66M3DJ-GA7GGT5-B3C2XME-LDTVBAH";
      };
    };
    folders = {
      "/var/lib/syncthing/sync-brendon" = {
        id = "mxmnr-opswh";
        devices = [ "calyx-phone" ];
        ignorePerms = true;
      };
    };
  };

  systemd.services.syncthing.serviceConfig.UMask = "0007";

  users.users.syncthing = {
    homeMode = "770";
    createHome = true;
  };
}
