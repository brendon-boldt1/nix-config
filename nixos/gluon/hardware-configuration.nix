# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  boot.initrd.availableKernelModules =
    [ "xhci_pci" "ehci_pci" "ahci" "usbhid" "sd_mod" "rtsx_pci_sdmmc" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/5beee34d-120c-4b3a-82a5-88921cd7bce8";
    fsType = "btrfs";
    options = [ "subvol=@nixos-root" ];
  };

  boot.initrd.luks.devices."btrfs_clear".device =
    "/dev/disk/by-uuid/92918bae-34e7-41a1-a88c-b4dc5850a3b6";

  fileSystems."/nix" = {
    device = "/dev/disk/by-uuid/5beee34d-120c-4b3a-82a5-88921cd7bce8";
    fsType = "btrfs";
    options = [ "subvol=@nix" ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/5beee34d-120c-4b3a-82a5-88921cd7bce8";
    fsType = "btrfs";
    options = [ "subvol=@boot" ];
  };

  fileSystems."/boot/efi" = {
    device = "/dev/disk/by-uuid/45F1-21EC";
    fsType = "vfat";
  };

  fileSystems."/mnt/btrfs_pool" = {
    device = "/dev/disk/by-uuid/5beee34d-120c-4b3a-82a5-88921cd7bce8";
    fsType = "btrfs";
  };

  swapDevices = [ ];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp0s25.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp3s0.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode =
    lib.mkDefault config.hardware.enableRedistributableFirmware;
}
