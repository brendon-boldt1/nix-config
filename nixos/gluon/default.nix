{ inputs, outputs, lib, config, pkgs, ... }: {
  imports = [ ./hardware-configuration.nix ./syncthing.nix ../common.nix ];

  # boot.kernelPackages = pkgs.linuxPackages_zen;

  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernelParams = [
    # I think this fixes the double/triple touch issue after suspend.
    "psmouse.synaptics_intertouch=0"
  ];

  boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.loader.grub.enable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.devices = [ "nodev" ];
  boot.loader.grub.enableCryptodisk = true;
  boot.tmp.useTmpfs = true;

  boot.initrd.luks.devices."btrfs_clear".allowDiscards = true;
  services.fstrim.enable = true;

  # boot.initrd = {
  #   # extraFiles."/crypto_keyfile.bin".source = /boot/crypto_keyfile.bin;
  #   # prepend = ["${/boot/crypto_keyfile.cpio}"];
  #   # luks.devices."btrfs_clear".keyFile = "/keyfile";
  #   secrets."/crypto_keyfile.bin" = "/boot/crypto_keyfile.bin";
  # };

  swapDevices = [{ device = "/mnt/btrfs_pool/swapfile"; }];
  zramSwap.enable = true;

  fileSystems = let options = [ "compress=zstd" ];
  in {
    "/".options = options;
    "/mnt/btrfs_pool".options = options;
    "/nix".options = options;
  };

  networking.hostName = "gluon"; # Define your hostname.
  networking.wireless.enable = false;
  networking.networkmanager.enable = true;

  networking.firewall = {
    enable = true;
    allowedTCPPorts = [ 8100 ];
  };

  programs.sway.enable = true;
  xdg.portal.wlr.enable = true;
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  services.pipewire.enable = true;

  virtualisation = {
    oci-containers.backend = "podman";
    podman = {
      enable = true;
      dockerCompat = false;
    };
    docker.enable = true;
  };

  services = {
    udev.extraRules =
      let brightFile = "/sys/class/backlight/intel_backlight/brightness";
      in ''
        ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="${pkgs.coreutils}/bin/chgrp video ${brightFile}", RUN+="${pkgs.coreutils}/bin/chmod g+w ${brightFile}" 
      '';
    acpid.lidEventCommands = ''
      systemctl suspend
    '';
  };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  users.users.brendon = {
    isNormalUser = true;
    extraGroups = [ "wheel" "video" "syncthing" "adbusers" ];
  };

  environment = { systemPackages = with pkgs; [ mbuffer btrbk ]; };

  fonts.fonts = with pkgs; [ fantasque-sans-mono ];

  programs.adb.enable = true;
  programs.nix-ld.enable = true;

  services.btrbk.instances.default = {
    onCalendar = "Mon,Wed,Fri 12:00";
    settings = {
      stream_buffer = "256m";
      snapshot_preserve_min = "2d";
      snapshot_preserve = "14d";
      target_preserve_min = "no";
      target_preserve = "20d 10w";
      timestamp_format = "short";
      volume = {
        "/mnt/btrfs_pool" = {
          snapshot_dir = "./snapshots";
          target = {
            "ssh://pion.lan:22/mnt/btrfs_backup/btrbk/gluon" = {
              ssh_user = "btrbk";
              ssh_identity = "/var/lib/btrbk/.ssh/id_rsa";
            };
          };
          snapshot_create = "always";
          subvolume = {
            "@nixos-root" = { };
            "@boot" = { };
          };
        };
      };
    };
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  system.copySystemConfiguration = false;
  system.stateVersion = "22.05";
}
