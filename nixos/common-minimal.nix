{ inputs, outputs, lib, config, pkgs, ... }: {
  imports = [ ];

  nixpkgs.overlays = builtins.attrValues outputs.overlays;

  nix = {
    registry = lib.mapAttrs (_: value: { flake = value; }) inputs;
    nixPath = lib.mapAttrsToList (key: value: "${key}=${value.to.path}")
      config.nix.registry;
    settings = {
      experimental-features = "nix-command flakes";
      auto-optimise-store = true;
    };
  };

  programs.zsh.enable = true;
  users.defaultUserShell = pkgs.zsh;

  time.timeZone = "America/New_York";
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true; # use xkbOptions in tty.
  };

  networking.networkmanager.enable = true;
  networking.wireless.enable = false;

  ### GUI ###
  nixpkgs.config.pulseaudio = true;
  services.xserver = {
    enable = true;
    desktopManager = {
      xterm.enable = false;
      xfce.enable = true;
    };
    xkb.options = "ctrl:nocaps";
  };
  services.displayManager.defaultSession = "xfce";
  ### End GUI ###

  environment = {
    systemPackages = with pkgs; [ tmux git neovim midori st ];

    # TODO Turn neovim, zsh, tmux into modules
    etc = {
      "nix-config".source = ./..;
      "tmux.conf".text = ''
        set -g status-keys vi
        set -g mode-keys   vi
        unbind C-b
        set -g prefix C-Space
        bind -N "Send the prefix key through to the application" \
          Space send-prefix
        bind '"' split-window -c "#{pane_current_path}"
        bind % split-window -h -c "#{pane_current_path}"
      '';
    };
  };

}
