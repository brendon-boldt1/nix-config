# Build with nix build .#nixosConfigurations.live-iso.config.system.build.isoImage

{ inputs, outputs, lib, config, pkgs, ... }: {
  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  imports = [
    "${inputs.nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix"
    "${inputs.nixpkgs}/nixos/modules/installer/cd-dvd/channel.nix"
    ./common-minimal.nix
  ];

  isoImage.squashfsCompression = "zstd";
}
