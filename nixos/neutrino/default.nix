{ inputs, outputs, lib, config, pkgs, ... }: {
  imports = [ ./common-minimal.nix ];
  config.lib.isoFileSystems = {
    "/iso" = lib.mkImageMediaOverride {
      # device = "/dev/root";
      device = "/dev/disk/by-uuid/3CCB-71EC";
      neededForBoot = true;
      noCheck = true;
    };
  };

  networking.hostName = "neutrino";
  # networking.wireless.enable = false;
  # networking.networkmanager.enable = true;

  networking.firewall = {
    enable = true;
    allowedTCPPorts = [ 8100 ];
  };
}
