{ config, pkgs, lib, ... }:

let
  user = "brendon";
  hostname = "pion";
in {
  imports = [ ../common.nix ./hardware-config.nix ];

  fileSystems = {
    "/" = {
      device = lib.mkForce "/dev/disk/by-label/NIXOS_SD";
      options = [ "noatime" ];
    };
  };

  networking = { hostName = hostname; };

  services.openssh.enable = true;

  users = {
    users."${user}" = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
    };
  };

  environment.systemPackages = with pkgs; [ cryptsetup ];

  # Enable GPU acceleration
  hardware.raspberry-pi."4".fkms-3d.enable = true;

  system.stateVersion = "23.05";

  boot.tmpOnTmpfs = true;
  nix = {
    settings = {
      auto-optimise-store = true;
      trusted-users = [ "brendon" ];
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
    extraOptions = ''
      min-free = ${toString (100 * 1024 * 1024)}
      max-free = ${toString (1024 * 1024 * 1024)}
    '';
  };

  services.btrbk.sshAccess = let
    roles = [ "info" "source" "target" "delete" "snapshot" "send" "receive" ];
  in [
    {
      key =
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDW4ZlzC984ruoq1Uj5Fe1543aN3wuq8sfmX8sJ9cu4R6A2aOP/RjyIEOaX0fOz3+ckS/ErsV+hGiDBE1SIAsfonw3sXCjyb9u9tRj6TiGuPkMdRUM0HaZVIf3F2EAdnWJpRw8lL7FPyD3QVUW6T8YXliwMoYnMEQT+b7El+p6yj3toV+wO10wSV6e+zGP66a/pPNK8zRbkQIsjoLkfjgN5PMS+ZIjN9THa5dk/6j/nF+MY+Wwa4fXBqIwvBTdG4JBJRhj0cywujmsyyYXLgUN0mcX5B8Mt9y7Cg9m1xsVUh+55C2OeslhCHPlgvn8xMUKKh5QB5elywXecvNREjDWVK1nqkHMQvu3rbiFFIosENcVWy5MNJ2Q9vjOhRvAvCRMr/Oi+xHMnJjcR9yz94xLE7GeEfsafFf6IvFS7gDhAsCwp7XNJuTA2rWZAx4qSmUshRnGXHp44McbZSds92YlNNuO601AVdXX5HqDwvHKX+4+U7m6lElnjaeW7vTTp5E0= btrbk@gluon";
      roles = roles;
    }
    {
      key =
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDgxDfSo9yH0Ib6qckk0qDCxlu59RHkzgkyxcG7vU2V7hiesjZekNCFhyhkahMHoPGm4bwxy1buEPFAHVno3F8G2NXUKUmcoo3Nbvzh5LxMwfXfQdH9ujOpuhJyjL7Rd7bvU+plP+m1IPp1ZuULB4XiPUb5X9yh9uAR1qJWOjfg5KHhjlsFLL/zh9MLNO9rCpBxEOBZ9QuPYy33WZxyPJQy75CRtLtQQDasqCThzRhbIgMeimGVVKTC8bVLch1WaLmr6p2166yGtl+04uAAPyIgydZgFntV8IXJ3JdIZnJz6t4PiTuRCLpEAVeOi95Ms0hM+UXImF9rviF7a0LfqinkPUQX3U9qeA6F4lpa3BVyAJoPNvBxMxqinzf4sPDk/zwdsRGaPF1PKtNsw4XVjMHe7rEuSK+V0Ayr9Gwi4ztRr8Q8FL6EZZOO4DEGPztLWqhIrPGzgRkCv6xP5ztMeN+R8kETmVX5yINsSyZk5G+UuuJwT/pJaY3CLpV1tVShUdk= btrbk@pentaquark";
      roles = roles;
    }
  ];
}
