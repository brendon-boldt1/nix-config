{ inputs, outputs }:
let
  nixpkgs = inputs.nixpkgs;
  hardware = inputs.hardware;

in {
  gluon = nixpkgs.lib.nixosSystem {
    specialArgs = { inherit inputs outputs; };
    modules = [ ./gluon ];
  };

  meson = nixpkgs.lib.nixosSystem {
    specialArgs = { inherit inputs outputs; };
    modules = [ ./meson hardware.nixosModules.lenovo-thinkpad-t480s ];
  };

  pion = nixpkgs.lib.nixosSystem {
    specialArgs = { inherit inputs outputs; };
    modules = [ ./pion hardware.nixosModules.raspberry-pi-4 ];
  };

  pentaquark = nixpkgs.lib.nixosSystem {
    specialArgs = { inherit inputs outputs; };
    modules = [ ./pentaquark ];
  };

  live-iso = nixpkgs.lib.nixosSystem {
    specialArgs = { inherit inputs outputs; };
    modules = [ ./live-iso.nix ];
  };
}
