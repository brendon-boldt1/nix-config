{ inputs, outputs, lib, config, pkgs, ... }: {
  system.stateVersion = "23.05";
  imports = [ ./hardware-configuration.nix ../common.nix ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/900a405f-b49d-4fef-9746-0aa6897cd4b4";
    fsType = "btrfs";
    options = [ "subvol=@nixos-root" ];
  };
  fileSystems."/mnt/btrfs" = {
    device = "/dev/disk/by-uuid/900a405f-b49d-4fef-9746-0aa6897cd4b4";
    fsType = "btrfs";
  };

  nixpkgs.config.allowUnfree = true;

  networking.hostName = "pentaquark";
  networking.wireless.enable = false;
  networking.networkmanager.enable = true;

  sound.enable = true;
  hardware.pulseaudio.enable = true;
  services.pipewire.enable = true;

  services.xserver = {
    enable = true;
    desktopManager = {
      xterm.enable = false;
      xfce.enable = true;
    };
    displayManager.defaultSession = "xfce";
  };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  users.mutableUsers = true;

  users.users.brendon = {
    isNormalUser = true;
    extraGroups = [ "wheel" "video" ];
  };

  users.users.elizabeth = {
    isNormalUser = true;
    extraGroups = [ "wheel" "video" ];
  };

  environment = {
    systemPackages = with pkgs;
      [ mbuffer ] ++ import ./packages.nix { inherit pkgs; };
  };

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  services.btrbk.instances.btrbk = {
    onCalendar = "Mon 14:00";
    settings = {
      stream_buffer = "256m";
      snapshot_preserve_min = "2d";
      snapshot_preserve = "14d";
      target_preserve_min = "no";
      target_preserve = "20d 10w";
      timestamp_format = "short";
      volume = {
        "/mnt/btrfs" = {
          snapshot_dir = "./snapshots";
          target = {
            "ssh://pion.lan:22/mnt/btrfs_backup/btrbk/pentaquark" = {
              ssh_user = "root";
              ssh_identity = "/root/.ssh/id_rsa";
            };
          };
          snapshot_create = "always";
          subvolume = {
            "@nixos-root" = { };
            "@boot" = { };
          };
        };
      };
    };
  };
}
