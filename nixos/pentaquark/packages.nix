{ pkgs }:
with pkgs; [
  discord
  firefox
  gimp
  signal-desktop
  slack
  texlive.combined.scheme-full
  texstudio
  libreoffice-still
  zip
]
