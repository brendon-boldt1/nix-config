{ inputs, outputs, lib, config, pkgs, ... }: {
  imports = [
    # ./hardware-configuration.nix
    ../common.nix
  ];

  boot.loader.efi.canTouchEfiVariables = true;

  boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.loader.grub.enable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.devices = [ "nodev" ];
  boot.tmp.useTmpfs = true;

  hardware.opengl.enable = true;

  # swapDevices = [{ device = "/mnt/btrfs_pool/swapfile"; }];
  zramSwap.enable = true;

  fileSystems = let options = [ "compress=zstd" ];
  in {
    "/".options = options;
    "/mnt/btrfs_pool".options = options;
    "/nix".options = options;

    "/mnt/btrfs_pool" = {
      device = "/dev/mapper/internal-btrfs";
      fsType = "btrfs";
    };

  };

  networking.hostName = "ion";
  networking.wireless.enable = false;
  networking.networkmanager.enable = true;

  networking.firewall = {
    enable = true;
    allowedTCPPorts = [ 8100 ];
  };

  programs.sway.enable = true;
  xdg.portal.wlr.enable = true;
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  services.pipewire.enable = true;

  services = {
    printing = {
      enable = true;
      drivers = [ pkgs.brlaser ];
    };
    avahi = {
      enable = true;
      nssmdns = true;
      openFirewall = true;
    };
  };

  users.users.brendon = {
    isNormalUser = true;
    extraGroups = [ "wheel" "video" ];
  };

  environment = { systemPackages = with pkgs; [ mbuffer btrbk ]; };

  fonts.packages = with pkgs; [ fantasque-sans-mono ];

  programs.nix-ld.enable = true;

  services.btrbk.instances.default = {
    onCalendar = "Mon,Wed,Fri 12:00";
    settings = {
      stream_buffer = "256m";
      snapshot_preserve_min = "2d";
      snapshot_preserve = "14d";
      target_preserve_min = "5d";
      target_preserve = "20d 10w";
      timestamp_format = "short";
      volume = {
        "/mnt/btrfs_pool" = {
          snapshot_dir = "./snapshots";
          target = {
            "ssh://pion.lan:22/mnt/btrfs_backup/btrbk/meson" = {
              ssh_user = "btrbk";
              ssh_identity = "/var/lib/btrbk/.ssh/id_rsa";
            };
          };
          snapshot_create = "always";
          subvolume = { "@nixos-root" = { }; };
        };
      };
    };
  };

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  system.copySystemConfiguration = false;
  system.stateVersion = "23.05";
}
