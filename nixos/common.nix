{ inputs, outputs, lib, config, pkgs, ... }: {
  imports = [ ];

  nixpkgs.overlays = builtins.attrValues outputs.overlays;

  nix = {
    registry = lib.mapAttrs (_: value: { flake = value; }) inputs;
    nixPath = lib.mapAttrsToList (key: value: "${key}=${value.to.path}")
      config.nix.registry;
    settings = {
      experimental-features = "nix-command flakes";
      auto-optimise-store = true;
    };
  };

  hardware.opengl.enable = true;

  time.timeZone = lib.mkDefault "America/New_York";
  i18n.defaultLocale = "en_US.UTF-8";
  services.xserver.xkb.options = "ctrl:nocaps";
  console = {
    font = "Lat2-Terminus16";
    # keyMap = "us";
    useXkbConfig = true; # use xkbOptions in tty.
  };

  programs.zsh.enable = true;
  users.defaultUserShell = pkgs.zsh;
  programs.mosh.enable = true;

  environment = {
    systemPackages = with pkgs; [
      btrfs-progs
      dnsutils
      file
      gdu
      git
      iotop
      lynx
      mbuffer
      neovim
      nmap
      sshfs
      tmux
      wget
    ];
    variables = {
      EDITOR = "nvim";
      VISUAL = "nvim";
      MOSH_ESCAPE_KEY = "";
    };
    pathsToLink = [ "/share/zsh" ];
  };

  hardware.pulseaudio.extraConfig = ''
    load-module module-echo-cancel use_master_format=1 aec_method=webrtc aec_args="analog_gain_control=0 digital_gain_control=1" source_name=echoCancel_source sink_name=echoCancel_sink
    set-default-source echoCancel_source
    set-default-sink echoCancel_sink
  '';

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  networking.nameservers = [ "1.1.1.3" "1.0.0.3" ];

  system.copySystemConfiguration = false;
}
