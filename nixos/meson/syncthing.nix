{ config, pkgs, lib, ... }:

{
  services.syncthing = {
    enable = true;
    overrideDevices = true;
    overrideFolders = true;
    settings = {
      devices = {
        divestos-phone = {
          id =
            "3L2C5RN-HWG6B22-3CPEPC4-KGLZWXO-F25YHZH-XBT5T4A-W34HU5D-26A3LQD";
        };
      };
      folders = {
        "/var/lib/syncthing/sync-brendon" = {
          id = "mxmnr-opswh";
          devices = [ "divestos-phone" ];
          ignorePerms = true;
        };
        "/var/lib/syncthing/SeedVault" = {
          id = "7h0v2-v4lka";
          devices = [ "divestos-phone" ];
          ignorePerms = true;
        };
      };
    };
  };

  systemd.services.syncthing.serviceConfig.UMask = "0007";

  users.users.syncthing = {
    homeMode = "770";
    createHome = true;
  };
}
