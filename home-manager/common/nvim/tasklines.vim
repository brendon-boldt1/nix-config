syn match ttHeading   '^# .*'
syn match ttItem      '^ *\. .*'
syn match ttUrgent    '^ *! .*'
syn match ttComplete  '^ *x .*'
syn match ttVoid      '^ */ .*'
syn match ttProgress  '^ *- .*'
syn match ttQuestion  '^ *? .*'
syn region ttBlockL1 start='^# ' end='^\ze#' fold transparent
" syn region ttBlockL1 start='^[-/.?!x]' end='^$' end='\ze[^ ]' fold transparent
" syn match ttDay '^  \d\d[umtwrfs]'
" syn match ttTime '^    \d\d\d\d\(-\d\d\d\d\)\?'
" syn region ttWeekBlock start='W' end='\zeW' fold transparent
" syn region ttDayBlock start='^  \d\d[umtwrfs]' end='^\zeW' end='^\ze  \d' fold transparent

let b:current_syntax = "tasktree"

hi ttHeading ctermfg=blue guifg=cyan
hi ttItem ctermfg=white guifg=white
hi ttComplete ctermfg=darkgray guifg=#606060
" hi ttProgress ctermfg=lightblue
hi ttProgress ctermfg=gray guifg=#a0a0a0
hi ttVoid ctermfg=darkgray guifg=#606060
hi ttQuestion ctermfg=gray guifg=#a0a0a0
hi ttUrgent ctermfg=red cterm=Bold gui=bold guifg=red
hi def link Fold ttHeading

" setlocal foldtext=MyFoldText()
" function MyFoldText()
"   let lines = getline(v:foldstart, v:foldend)
"   let out = lines[0]
"   for line in lines[1:]
"     let sub = substitute(line, '^ \+', ' ', '')
"     let out = out . sub
"   endfor
"   " let sub = substitute(line, '/\*\|\*/\|{{{\d\=', '', 'g')
"   return out
" endfunction

setlocal fdm=syntax
