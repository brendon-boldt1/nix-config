{ inputs, outputs, config, pkgs, lib, ... }:

{
  programs.neovim = {
    enable = true;
    plugins = with pkgs.vimPlugins; [
      # neoclide/coc.nvim', {'branch': 'release', 'for': ['javascript', 'typescript', 'jsx', 'tsx', 'typescriptreact'] }
      ale
      ctrlp-vim
      haskell-vim
      markdown-preview-nvim
      nvim-treesitter.withAllGrammars
      todo-txt-vim
      typescript-vim
      vim-autoformat
      vim-cool
      fastfold
      vim-javascript
      vim-jsx-typescript
      vim-monokai
      vim-nix
      vim-sensible
      vimtex
    ];
    extraConfig = builtins.readFile ./init.vim;
    extraLuaConfig = builtins.readFile ./init.lua;
  };

  xdg.configFile = {
    "nvim/syntax/callines.vim".source = ./callines.vim;
    "nvim/syntax/tasklines.vim".source = ./tasklines.vim;
  };
}
