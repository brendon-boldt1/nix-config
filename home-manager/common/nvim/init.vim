"" general
set nocompatible
" Commenting these out.  They may be unnecessary
" filetype off
" syntax on
" filetype plugin indent on
set encoding=utf-8 hidden ttyfast lazyredraw incsearch fdm=syntax
let g:Imap_FreezeImap=1

"" input
set ts=2 sw=2 softtabstop=2 et noshiftround js nrformats-=alpha backspace=indent,eol,start matchpairs+=<:>
let mapleader = " "
map <Leader>s :%s/\<<C-r><C-w>\>//g<Left><Left>
nnoremap / /\v
vnoremap / /\v
inoremap <F1> <ESC>:set invfullscreen<CR>a
nnoremap <F1> :set invfullscreen<CR>
vnoremap <F1> :set invfullscreen<CR>

"" appearance
set ru nu rnu formatoptions=tcqrn1 showmatch laststatus=2 modelines=0 breakindent showbreak=.. showmode showcmd hlsearch cursorline cursorcolumn listchars=tab:▸\ ,eol:¬ t_Co=256 scrolloff=3 background=dark mouse=
hi CursorLine cterm=underline ctermbg=none
hi CursorColumn cterm=none ctermbg=darkgray
hi SpellBad ctermbg=darkred ctermfg=white
hi Pmenu ctermbg=black ctermfg=magenta

"" filetype
au FileType text setl nocursorline nocursorcolumn signcolumn=no fdm=manual
au FileType markdown setl nocursorline nocursorcolumn signcolumn=no spell
au FileType tex setl nocursorcolumn spell isk+=\- indentexpr=
au FileType todo setl cursorline nocursorcolumn nospell nrformats+=alpha
au FileType callines setl cul nocuc nospell ts=2 sw=2
au FileType tasklines setl cul nocuc nospell ts=2 sw=2
au FileType gitcommit setl spell
au FileType rst setl textwidth=79
" augroup javascript_folding
"     au!
"     au FileType javascript setlocal foldmethod=syntax
" augroup END
autocmd BufNewFile,BufRead *.tsx set syntax=typescript
autocmd BufNewFile,BufRead *.cal,*.cl.txt,*.cl,cl.txt :set filetype=callines
autocmd BufNewFile,BufRead *.tt,*.tl.txt,*tl,tl.txt :set filetype=tasklines
autocmd BufNewFile,BufRead *.tsx,*.jsx set ft=typescriptreact

"" plugin
let g:vimtex_view_method = 'zathura'
let g:vimtex_fold_enabled = 1
let g:ctrlp_custom_ignore = 'node_modules\|env'
hi CocInfoFloat ctermfg=yellow
let g:ale_sign_column_always = 1
let g:ale_linters = {
\    'python': ['mypy'],
\}
nnoremap <silent> <C-b> :NERDTreeToggle<CR>
let g:coc_global_extensions = ['coc-css', 'coc-html', 'coc-json', 'coc-prettier', 'coc-tsserver']
runtime! macros/matchit.vim
color monokai
