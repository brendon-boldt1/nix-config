" if exists("b:current_syntax")
"     finish
" endif

syn match calWeek '^W\.\d\d\.\d\d'
syn match calDay '^  \d\d[umtwrfs]'
syn match calTime '^    \d\d\d\d\(-\d\d\d\d\)\?'
syn region calWeekBlock start='W' end='\zeW' fold transparent
syn region calDayBlock start='^  \d\d[umtwrfs]' end='^\zeW' end='^\ze  \d' fold transparent

let b:current_syntax = "cal"

hi def link calWeek PreProc
hi def link calDay Statement
hi def link calTime Constant

setlocal fdm=syntax
