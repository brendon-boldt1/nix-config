{ inputs, outputs, config, pkgs, lib, ... }:

{
  programs.home-manager.enable = true;
  imports = [ ./nvim inputs.nix-index-database.hmModules.nix-index ];

  nixpkgs = { overlays = builtins.attrValues outputs.overlays; };

  home = {
    sessionVariables = {
      EDITOR = "nvim";
      VISUAL = "nvim";
      LESS = "-FR";
      MAMBA_ROOT_PREFIX = "~/.micromamba";
    };
    sessionPath = [ "$HOME/.local/bin" ];
    packages = with pkgs; [
      bc
      gdu
      gnupg
      inotify-tools
      jq
      micromamba
      nixpkgs-fmt
      nnn
      python311
      shellcheck
      tree
      unzip
      yq
      zip
    ];
  };

  programs = {
    direnv = {
      enable = true;
      nix-direnv.enable = true;
      enableZshIntegration = true;
    };

    git = {
      enable = true;
      lfs.enable = true;
      delta.enable = true;
      aliases = {
        co = "checkout";
        br = "branch";
        ci = "commit";
        st = "status";
      };
      userEmail = lib.mkDefault "brendon.mb@protonmail.com";
      userName = "Brendon Boldt";
      extraConfig = { credential.helper = "store"; };
    };
    tmux = {
      enable = true;
      clock24 = true;
      keyMode = "vi";
      newSession = true;
      shortcut = "Space";
      extraConfig = ''
        bind '"' split-window -c "#{pane_current_path}"
        bind % split-window -h -c "#{pane_current_path}"
        set-option -s escape-time 0
        set-option -g history-limit 4096
        set-option -g display-time 4000
        set-option -g default-terminal "tmux-256color"
      '';
    };
    zsh = {
      enable = true;
      defaultKeymap = "emacs";
      initExtra = ''
        autoload -U select-word-style
        select-word-style bash
        setopt nullglob
        DISABLE_UNTRACKED_FILES_DIRTY=true
        SHARED_HISTORY=false
        PROMPT='%F{green}%m:%2d%f> '
        RPROMPT='%(?..%?)'
        zstyle ':completion:*' matcher-list ""
      '' + builtins.readFile ./init.shrc;
      oh-my-zsh = {
        enable = true;
        theme = "robbyrussell";
      };
    };
    bash = {
      enable = true;
      initExtra = ''
        function watch-file {
          cmd=$1
          shift
          files=$@
          while inotifywait -qqemodify -emove_self $files; do clear; sh -c "$cmd"; done
        }
        colors=($(seq 1 6 | shuf))
          export PS1="\[\e[1;3''${colors[0]}m\][\W]\[\e[1;3''${colors[1]}m\]\$\[\e[0m\] "
        # source ~/.git-completion.bash
      '' + builtins.readFile ./init.shrc;
    };

    nix-index-database.comma.enable = true;
    nix-index.enable = false;

    atuin = {
      enable = true;
      enableZshIntegration = true;
    };
  };

  xdg.configFile = {
    "nix/nix.conf".text = ''
      max-jobs = auto
    '';
  };

  xdg.userDirs = { enable = true; };

  xdg.mimeApps.enable = true;
  xdg.mimeApps.defaultApplications = {
    "text/plain" = [ "nvim.desktop" ];
    "text/markdown" = [ "nvim.desktop" ];
    "application/json" = [ "nvim.desktop" ];
  };
}
