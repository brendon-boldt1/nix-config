{ inputs, outputs }:
let
  nixpkgs = inputs.nixpkgs;
  hmConfig = inputs.home-manager.lib.homeManagerConfiguration;
  cmu = (extraMod: {
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
    extraSpecialArgs = { inherit inputs outputs; };
    modules = [ extraMod ./cmu.nix ];
  });
  babel = hmConfig (cmu ({
    home = {
      homeDirectory = "/home/bboldt";
      shellAliases = { nvim = "nvim --noplugin"; };
      sessionVariables = {
        EDITOR = nixpkgs.lib.mkForce "nvim --noplugin";
        VISUAL = nixpkgs.lib.mkForce "nvim --noplugin";
      };
    };
  }));
in {
  "brendon@gluon" = hmConfig {
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
    extraSpecialArgs = { inherit inputs outputs; };
    modules = [ ./personal/gluon.nix ];
  };

  "brendon@meson" = hmConfig {
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
    extraSpecialArgs = { inherit inputs outputs; };
    modules = [ ./personal/meson.nix ];
  };

  "brendon@pion" = hmConfig {
    pkgs = nixpkgs.legacyPackages.aarch64-linux;
    extraSpecialArgs = { inherit inputs outputs; };
    modules = [ ./pion.nix ];
  };

  "brendon@pentaquark" = hmConfig {
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
    extraSpecialArgs = { inherit inputs outputs; };
    modules = [ ./pentaquark.nix ];
  };

  "aws" = hmConfig {
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
    extraSpecialArgs = { inherit inputs outputs; };
    modules = [
      ./common
      {
        home.homeDirectory = "/root";
        home.stateVersion = "22.11";
        home.username = "root";
      }
    ];
  };

  "bboldt@patient.lti.cs.cmu.edu" =
    hmConfig (cmu ({ home.homeDirectory = "/usr0/home/bboldt"; }));
  "bboldt@agent.lti.cs.cmu.edu" =
    hmConfig (cmu ({ home.homeDirectory = "/usr0/home/bboldt"; }));
  "bboldt@lor.lti.cs.cmu.edu" =
    hmConfig (cmu ({ home.homeDirectory = "/usr2/home/bboldt"; }));
  "bboldt@schank.lti.cs.cmu.edu" =
    hmConfig (cmu ({ home.homeDirectory = "/usr0/home/bboldt"; }));
  "bboldt@chronos.lti.cs.cmu.edu" =
    hmConfig (cmu ({ home.homeDirectory = "/home/bboldt"; }));
  "bboldt@babel-login-1.lti.cs.cmu.edu" = babel;
  "bboldt@babel-login-2.lti.cs.cmu.edu" = babel;
  "bboldt@babel-login-3.lti.cs.cmu.edu" = babel;
}
