{ inputs, outputs, config, pkgs, lib, ... }:

{
  home.username = "brendon";
  home.homeDirectory = "/home/brendon";
  home.stateVersion = "23.05";

  imports = [ ./common ];
}
