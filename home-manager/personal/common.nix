{ inputs, outputs, config, pkgs, lib, ... }:

{
  home.username = "brendon";
  home.homeDirectory = "/home/brendon";
  home.stateVersion = "22.05";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  imports = [ ../common ./sway.nix ];

  nixpkgs = {
    config = {
      allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [ "slack" ];
    };
  };

  home = {
    sessionVariables = {
      MOZ_ENABLE_WAYLAND = "1";
      NIXOS_OZONE_WL = "1";
    };
    sessionPath = [ "$HOME/.local/bin" ];
    shellAliases = { zathuraf = "&>/dev/null zathura --fork"; };
    packages = with pkgs; [
      # sc-im # Security vulnerability (libxls)
      # zotero # Security vulnerability (libvpx)
      atop
      bemenu
      chromium
      cool-retro-term
      droopy
      feh
      firefox
      gimp
      gnumake
      keepassxc
      kitty
      libreoffice-still
      light
      pdfpc
      peaclock
      pulsemixer
      signal-desktop
      slack
      st
      sway-contrib.grimshot
      vlc
      wdisplays
      wl-clipboard
      wvkbd
      xdg-utils
    ];
  };

  programs = {
    alacritty = {
      enable = true;
      settings.general.import =
        [ "${inputs.alacritty-theme}/themes/monokai.toml" ];
      # settings.font.normal.family = "Fantasque Sans Mono";
      settings.font.normal.family = "Iosevka";
    };
    tmux.extraConfig = ''
      set -g status-style fg=white,bg=#444444
    '';
    zathura.enable = true;
    ranger = {
      enable = true;
      extraConfig = "set preview_images_method kitty";
    };

    zsh = {
      initExtra = ''
        PROMPT='%F{green}%2d%f> '
      '';
    };

    texlive = {
      enable = true;
      extraPackages = tpkgs: { inherit (tpkgs) scheme-full; };
    };

    obs-studio = {
      enable = true;
      plugins = with pkgs.obs-studio-plugins; [
        wlrobs
        obs-backgroundremoval
        obs-pipewire-audio-capture
      ];
    };
  };

  xdg.userDirs = {
    enable = true;
    desktop = "${config.home.homeDirectory}/personal/misc/Desktop";
    download = "${config.home.homeDirectory}/.cache/download";
  };

  xdg.mimeApps.enable = true;
  xdg.mimeApps.defaultApplications = let browser = "firefox.desktop";
  in {
    "application/pdf" =
      [ "org.pwmt.zathura-pdf-mupdf.desktop" "firefox.desktop" ];
    "application/json" = [ "nvim.desktop" ];
    "application/x-extension-htm" = [ "${browser}" ];
    "application/x-extension-html" = [ "${browser}" ];
    "application/x-extension-shtml" = [ "${browser}" ];
    "application/x-extension-xht" = [ "${browser}" ];
    "application/x-extension-xhtml" = [ "${browser}" ];
    "application/xhtml+xml" = [ "${browser}" ];
    "text/html" = [ "${browser}" ];
    "text/markdown" = [ "nvim.desktop" ];
    "text/plain" = [ "nvim.desktop" ];
    "x-scheme-handler/chrome" = [ "${browser};" ];
    "x-scheme-handler/http" = [ "${browser};" ];
    "x-scheme-handler/https" = [ "${browser};" ];
  };

  programs.git.includes = [{ contents = { init.defaultBranch = "main"; }; }];

  # Fixes for virt-manager
  dconf.settings = {
    "org/virt-manager/virt-manager/connections" = {
      autoconnect = [ "qemu:///system" ];
      uris = [ "qemu:///system" ];
    };
  };
  home.pointerCursor.gtk.enable = true;
  home.pointerCursor.package = pkgs.vanilla-dmz;
  home.pointerCursor.name = "DMZ-Black";
}
