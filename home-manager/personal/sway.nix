{ config, pkgs, lib, ... }:

let locker = "${pkgs.swaylock}/bin/swaylock";
in rec {
  wayland.windowManager.sway = {
    enable = true;
    config = {
      modifier = "Mod4";
      input = {
        "*" = {
          xkb_options = "ctrl:nocaps";
          tap = "enabled";
          accel_profile = "adaptive";
          pointer_accel = "0.0";
        };
        "type:touchpad" = {
          accel_profile = "adaptive";
          pointer_accel = "0.0";
        };
        "type:keyboard" = {
          repeat_delay = "500";
          repeat_rate = "100";
        };
      };
      seat.seat0.hide_cursor = "when-typing enable";
      output = {
        "*" = { bg = "/dev/null fill #000000"; };
        HDMI-A-1 = { pos = "0 0"; };
        DP-2 = { pos = "0 0"; };
        eDP-1 = { pos = "1680 100"; };
      };
      fonts = {
        names = [ "Iosevka" ];
        size = 9.0;
      };
      terminal = "st -f 'Fantasque Sans Mono-12'";
      keybindings = let mod = config.wayland.windowManager.sway.config.modifier;
      in lib.mkOptionDefault {
        "${mod}+Shift+f" = "exec ${pkgs.firefox}/bin/firefox";
        "${mod}+d" = "exec ${pkgs.bemenu}/bin/bemenu-run";
        "XF86AudioRaiseVolume" =
          "exec pactl set-sink-volume @DEFAULT_SINK@ +5%";
        "XF86AudioLowerVolume" =
          "exec pactl set-sink-volume @DEFAULT_SINK@ -5%";
        "XF86AudioMute" = "exec pactl set-sink-mute @DEFAULT_SINK@ toggle";
        "XF86MonBrightnessUp" = "exec light -A 10";
        "XF86MonBrightnessDown" = "exec light -U 10";
        "XF86Display" = "exec ${locker}";
        "Pause" = "exec ${locker}";
        "${mod}+0" = "workspace $ws10";
        "${mod}+Shift+0" = "move container to workspace $ws10";
        "Print" = "exec grimshot copy area";
      };
      bars = [{
        mode = "hide";
        trayOutput = "primary";
        statusCommand = "${pkgs.i3status}/bin/i3status";
        fonts = {
          names = [ "Iosevka" ];
          # style = "Bold Semi-Condensed";
          size = 9.0;
        };
      }];
    };

    extraConfig = ''
      set $ws10 10
    '';
  };

  programs.swaylock.settings = {
    daemonize = true;
    ignore-empty-password = true;
    color = "222222";
    text-clear-color = "00000000";
    text-ver-color = "00000000";
    text-wrong-color = "00000000";
  };

  services.swayidle = {
    enable = true;
    events = [{
      event = "before-sleep";
      command = locker;
    }
    # { event = "lock"; command = "lock"; }
    # { event = "after-resume"; command = "swaymsg 'output * dpms on'"; }
      ];
    timeouts =
      let dpms = state: "${pkgs.sway}/bin/swaymsg 'output * dpms ${state}'";
      in [
        {
          timeout = 600;
          command = dpms "off";
          resumeCommand = dpms "on";
        }
        {
          timeout = 610;
          command = locker;
        }
        {
          timeout = 10;
          command =
            "if ${pkgs.procps}/bin/pgrep -x swaylock; then ${dpms "off"}; fi";
          resumeCommand = dpms "on";
        }
      ];
  };

  services.gammastep = {
    enable = true;
    latitude = 40.44;
    longitude = -80.0;
    temperature.day = 6500;
  };

  systemd.user = {
    services.ensure-active-display = let
      ead = pkgs.writeShellApplication {
        name = "ensure-active-display";
        runtimeInputs = with pkgs; [ sway gnugrep coreutils ];
        text = ''
          function get_display_count {
            swaymsg -t get_outputs | grep -c '"name'
          }
          prev=$(get_display_count)
          while :; do
            current=$(get_display_count)
            if [[ $current < $prev ]] && [[ $current == 1 ]]; then
              swaymsg output eDP-1 enable
            fi
            prev=$current
            sleep 2
          done
        '';
      };
    in {
      Unit = { Description = "Ensure at least one display is active."; };
      Install.WantedBy = [ "default.target" ];
      Service = { ExecStart = "${ead}/bin/ensure-active-display"; };
    };
    timers.ensure-active-display = { };
  };
}
