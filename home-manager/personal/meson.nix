{ inputs, outputs, config, pkgs, lib, ... }:

{
  imports = [ ./common.nix ];

  wayland.windowManager.sway.config.output.eDP-1.scale = toString (5 / 4.0);
}
