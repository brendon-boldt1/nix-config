{ inputs, outputs, config, pkgs, lib, ... }:

{
  home.username = "bboldt";
  home.stateVersion = "22.11";

  programs.home-manager.enable = true;

  imports = [ ./common ];

  home = { packages = with pkgs; [ ]; };

  programs.tmux.shell = "${pkgs.zsh}/bin/zsh";

  programs.zsh.initExtraFirst = ''
    source ~/.nix-profile/etc/profile.d/nix.sh
  '';

  programs.bash.bashrcExtra = ''
    source ~/.nix-profile/etc/profile.d/nix.sh
  '';

  xdg.configFile = {
    "nix/nix.conf".text = ''
      experimental-features = nix-command flakes
    '';
  };

  programs.git.userEmail = "bboldt@cs.cmu.edu";
}
