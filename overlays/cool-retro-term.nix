self: super: {
  cool-retro-term = super.cool-retro-term.overrideAttrs (old: rec {
    version = "1.2.0";
    src = super.fetchFromGitHub {
      owner = "swordfish90";
      repo = "cool-retro-term";
      rev = version;
      sha256 = "PewHLVmo+RTBHIQ/y2FBkgXsIvujYd7u56JdFC10B4c=";
    };
    buildInputs = old.buildInputs ++ [ super.libsForQt5.qtquickcontrols2 ];
  });
  libsForQt5 = super.libsForQt5.overrideScope' (qself: qsuper: {
    qmltermwidget = qsuper.qmltermwidget.overrideAttrs (old: {
      src = super.fetchFromGitHub {
        repo = "qmltermwidget";
        owner = "Swordfish90";
        rev = "63228027e1f97c24abb907550b22ee91836929c5";
        sha256 = "aVaiRpkYvuyomdkQYAgjIfi6a3wG2a6hNH1CfkA2WKQ=";
      };
      version = "unstable-2022-09-16";
      patches = [ ];
    });
  });
}
